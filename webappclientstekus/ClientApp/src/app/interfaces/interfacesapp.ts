/**
 * Interface clients_ser_countries
 */
interface clients_ser_countries {
  idclient: number;
  name: string;
  idservice: number;
  service: string;
  idcountry: number;
  country: string;
  priceperhour: number
}

/**
 * Interface countservicepercountry
 */
interface countservicepercountry {
  namecountry: string;
  countofservice: number
}


/**
 * Interface viewmodelservicesbycountry
 */
interface viewmodelservicesbycountry {
  countofclien: number;
  countofservice: number;
  countserpercountry: countservicepercountry[]
}

/**
 * Interface services
 */
interface services {
  id: number;
  name: string;
  priceperhour: number
}

/**
 * Interface countries
 */
interface countries {
  id: number;
  name: string
}

/**
 * Interface vievmodelclientservicecountry
 */
interface vievmodelclientservicecountry
{
  clientssercount: clients_ser_countries[];
  countries: countries[];
  services: services[]
}

/**
 * Interface responseapi
 */
interface responseapi {
  mssg: string;
  response: any;
  saved:boolean
}

/**
 * Interface modelreqclientservice
 */
interface modelreqclientservice {
  nit: number;
  name: string;
  email: string;
  idservice: number;
  idcountry: number;
  cache: boolean
}
