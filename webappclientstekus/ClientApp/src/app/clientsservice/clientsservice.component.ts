import { Component, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { element } from 'protractor';
import { NgForm, FormBuilder, FormGroup, Validators, FormControl} from '@angular/forms';
import { faUser, faAddressCard } from '@fortawesome/free-solid-svg-icons';
import { ServicesComponent } from '../services/services.service';
import { catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';
import { formatDate } from '@angular/common';

/**
 * TypeScript clientsservice to clientsservice.component.html
 */
@Component({
  selector: 'clientsservice',
  templateUrl: './clientsservice.component.html'
})


export class ClientsSerivcesDataComponent {
  public viewclientssercount: vievmodelclientservicecountry = {
    clientssercount: [],
    services: [],
    countries: []
  };

  //Variables of component
  public allClients: clients_ser_countries[];
  public model: modelreqclientservice = {
    nit: null,
    name: null,
    email: null,
    idcountry: null,
    idservice: null,
    cache: false
  };

  listdatasave: modelreqclientservice[];

  skeytemporally: string = "";
  priceperhourd: number = 0;
  searchTerm: string;
  page = 1;
  pageSize = 4;
  collectionSize: number;
  currentRate = 8;
  /*BaseUrl: string;*/
  //FontAwesome Icons instance
  faUser = faUser;
  faAddressCard = faAddressCard;
  //


  //constructor(http: HttpClient, @Inject('BASE_URL') baseUrl: string) {
  //Constructor of component
  constructor(private serviciets: ServicesComponent) {
    this.serviciets.SendGetWOutParamObs('ClientServices/getlistclientsservices').subscribe((result: responseapi) => {
      if (result != null)
        this.loadForm(result, false);
    });
  }


  /**
   * Searches the client into grid details.
   * @param {string} value The value.
   */
  searchClient(value: string): void {
    this.viewclientssercount.clientssercount = this.allClients.filter((val) => val.name.toLowerCase().includes(value));
    this.collectionSize = this.viewclientssercount.clientssercount.length;
  }

  /**
   * Validate and send the form to save, service by client.
   * @param {NgForm} forma The varibles of form.
   */
  validarCF(formdata: NgForm) {
    if (formdata.invalid) {
      Object.values(formdata.controls).forEach(control => {
        control.markAllAsTouched();
      });
    }
    else {
      //Save Model
      let datasave: modelreqclientservice = (<modelreqclientservice>formdata.value);
      if (datasave.cache == null)
        (<modelreqclientservice>formdata.value).cache = false;
      if (datasave.cache) {
        //If is save temporally
        //Save temporally
        this.skeytemporally = null;
        this.skeytemporally = (datasave.nit.toString() + datasave.idservice.toString() + datasave.idcountry.toString());
        if (localStorage.getItem(this.skeytemporally) != null)
          localStorage.removeItem(this.skeytemporally);
        localStorage.setItem((datasave.nit.toString() + datasave.idservice.toString() + datasave.idcountry.toString()), JSON.stringify(datasave));
        alert(("Service " + datasave.idservice.toString() + " to client " + datasave.nit.toString() + " was success saved temporally!"));
        formdata.resetForm();
      }
      else {
        if (confirm("Do you save data ?")) {
          this.listdatasave = [];
          this.listdatasave.push(datasave);
          //Validate if there is entries temporally to save
          if (localStorage.length > 0) {
            if (confirm("There are temporary records to save. Do you save data temporally?")) {
              //for (var key in localStorage) {
              //  if(key != null)
              //    this.listdatasave.push(JSON.parse(localStorage.getItem(key)));
              //}
              for (let i = 0; i <= localStorage.length; i++) {
                if (localStorage.getItem(localStorage.key(i)) != null)
                  this.listdatasave.push(JSON.parse(localStorage.getItem(localStorage.key(i))));
              }
              localStorage.clear();
            }
          }
          //console.log(JSON.stringify((<modelreqclientservice>formdata.value)));
          this.serviciets.SendPOSTWParamObs("ClientServices/createservicebyclient", this.listdatasave).then((res: responseapi[]) => {
            /*        this.serviciets.SendPOSTWParamObs("ClientServices/createservicebyclient", (<modelreqclientservice>formdata.value)).subscribe((res: responseapi) => {*/
            //console.log("aqui" + res);
            if (res != null) {
              //if (res.mssg != null && !res.saved) {
              //  alert(res.mssg);
              //}
              //else if (res.mssg != null && res.saved) {
              console.log(res);
              //formdata.reset();
              let mssjj = "";
              res.forEach(i => {
                mssjj += (i.mssg + "\n");
              });

              if (res[(res.length - 1)].saved)
                this.loadForm(res[(res.length - 1)]);
              formdata.resetForm();
              this.model.cache = false;
              //alert(res.mssg);
              alert(mssjj);
            }

          }), catchError(error => {
            return throwError('Respuesta not found!');
          });
        }
      }
    }
  }


  /**
   * Load the form with data.
   * @param {responseapi} result The result are models necessary to load form.
   */
  loadForm(result: responseapi, savedata = true) {
    //if (result != null) {
    //  if (result.saved) {
    if (savedata)
      this.viewclientssercount.clientssercount = result.response != null ? result.response : [];
    
    else
      this.viewclientssercount = result.response != null ? result.response : [];
    this.refershGrid();
    //}
    //else
    //  alert(result.mssg);
    //}
  }

  /**
   * Refershes the grid and pagination when there is new data.
   */
  refershGrid() {
    this.collectionSize = (this.viewclientssercount.clientssercount != null && this.viewclientssercount.clientssercount.length > 0) ? this.viewclientssercount.clientssercount.length : 1;
    this.allClients = (this.viewclientssercount.clientssercount != null && this.viewclientssercount.clientssercount.length > 0) ? this.viewclientssercount.clientssercount : [];
  }


  /**
   * Method the options selected to filter service and fill the input price.
   * @param value The value form select.
   */
  onOptionsSelected(value) {

    /*(<HTMLInputElement>document.getElementById("txtPrice")).value =*/
    this.priceperhourd = value != null ? (this.viewclientssercount.services != undefined || this.viewclientssercount.services != null) ? this.viewclientssercount.services.find((val) => val.id == value).priceperhour : 0 : 0;

  }

  //onRowDblClick(value) {
  //  /*   console.log(value.rowData);*/
  //}


  /**
   * Delete all entries form service and clients.
   */
  deleteall() {
    if (confirm(("Do you like to delete all data from service by client in detalis?"))) {
      this.serviciets.SendDelete("ClientServices/deleteallrecordsserbycli").then((res: responseapi) => {
        /*console.log("resssss" + res);*/
        if (res.saved) {
          //this.viewclientssercount.clientssercount = null;
          this.viewclientssercount.clientssercount = [];
          this.refershGrid();
        }
      });
    }
  }
}
