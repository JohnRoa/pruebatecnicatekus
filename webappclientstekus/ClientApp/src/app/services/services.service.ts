import { Component, Inject, Injectable, NgModule  } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { promise } from 'protractor';

/**
 * TypeScript o Component of services to invoque controller
 */
@Injectable({
  providedIn: "root"
})


export class ServicesComponent {
  httpOptionsPlain = {
    headers: new HttpHeaders({
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    })
  };

  BaseUrl: string;
  constructor(private http: HttpClient, @Inject("BASE_URL") agbaseUrl: string) { this.BaseUrl = agbaseUrl; }

  /**
   * Sends the get w out parameter obs.
   * @param {string} route The route of the request.
   * @returns object Observable<any>
   */
  public SendGetWOutParamObs(route: string): Observable<any> {
    return this.http.get<Observable<any>>(this.BaseUrl + route).pipe(map((data: any) => {
      return data;
    }));
  }

  /**
   * Sends the post parameter obs.
   * @param {string} route The route.
   * @param {any} body The body are parameters.
   * @returns object Promise<any>
   */
  public SendPOSTWParamObs(route: string, body: any): Promise<any> {
    /*  public SendPOSTWParamObs(route: string, body: any): Observable<any> {*/
    //console.log("ruta", route, "body", body, JSON.stringify(body));
    //return this.http.post<Observable<any>>((this.BaseUrl + route), body, {
    //  headers: new HttpHeaders({
    //    'Accept': 'application/json',
    //    'Content-Type': 'application/json'
    //  }), responseType: 'json'}).pipe(map((data: any) => {
    //  return data;
    //}));
    return this.http.post((this.BaseUrl + route), body).toPromise().then((data: any) => {
      return data;
    });
  }


  /**
   * Sends the delete method.
   * @param {string} route The route is a parameter of request delete.
   * @returns object Promise<any>
   */
  public SendDelete(route: string): Promise<any> {
    return this.http.delete((this.BaseUrl + route)).toPromise().then((data: any) => {
      return data;
    });
  }
}


//alert(baseUrl);
//http.get<responseapi>(baseUrl + 'ClientServices/getservicesbycountry').subscribe(result => {
//  if (result != null) {
//    if (result.msg == null) {
//      if (result.response != null) {
//        this.serbycountry = result.response;
//        this.allCountries = this.serbycountry.countserpercountry;
//        console.log(result);
//      }
//    }
//    else
//      alert(result.msg);
//  }
//}, error => console.error(error));


    //http.get<responseapi>(baseUrl + 'ClientServices/getlistclientsservices').subscribe(result => {
    //  if (result != null) {
    //    if (result.msg == null) {
    //      console.log(result);
    //      this.viewclientssercount = result.response;
    //      this.collectionSize = this.viewclientssercount.clientssercount.length;
    //      this.allClients = this.viewclientssercount.clientssercount;
    //    }
    //    else
    //      alert(result.msg);
    //  }
    //}, error => console.error(error));

