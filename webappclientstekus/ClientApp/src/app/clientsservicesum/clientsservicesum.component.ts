import { Component, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { element } from 'protractor';
import { ServicesComponent } from '../services/services.service';

/**
 * Component serivces and clients by countries summary
 */
@Component({
  selector: 'clientsservicesum',
  templateUrl: './clientsservicesum.component.html'
})
export class ClientsSerivcesSumComponent {
  //Variables of component
  public serbycountry: viewmodelservicesbycountry = null;
  public allCountries: countservicepercountry[];
  searchTerm: string;
  page = 1;
  pageSize = 4;
  collectionSize: number;
  currentRate = 8;
  //

  //Constructor of component
  /*  constructor(http: HttpClient, @Inject('BASE_URL') baseUrl: string) {*/
  constructor(private serviciets: ServicesComponent) {
    this.serviciets.SendGetWOutParamObs('ClientServices/getservicesbycountry').subscribe((result: responseapi) => {
      if (result != null) {
        if (result.mssg == null) {
          if (result.response != null) {
            this.serbycountry = result.response;
            this.allCountries = this.serbycountry.countserpercountry;
            console.log(result);
          }
        }
        else
          alert(result.mssg);
      }
    });
  }


  /**
   * Searches the country.
   * @param {string} value The value.
   */
  searchCountry(value: string): void {
    this.serbycountry.countserpercountry = this.allCountries.filter((val) => val.namecountry.toLowerCase().includes(value));
    this.collectionSize = this.serbycountry.countserpercountry.length;
  }
}
