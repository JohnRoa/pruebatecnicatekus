import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { AppComponent } from './app.component';
import { NavMenuComponent } from './nav-menu/nav-menu.component';
import { HomeComponent } from './home/home.component';
import { ClientsSerivcesDataComponent } from './clientsservice/clientsservice.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ClientsSerivcesSumComponent } from './clientsservicesum/clientsservicesum.component';
import { FooterComponent } from './footer/footer.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { ServicesComponent } from './services/services.service';
//material
//import { MatFormFieldModule } from '@angular/material/form-field';
//import { MatInputModule } from '@angular/material/input';
//import { ListFilterPipe } from './listFilterPipe';
//import { DetailsComponent } from './detailscs/detailscs.component';
//import { MatPaginatorModule } from '@angular/material/paginator';

@NgModule({
  declarations: [
    AppComponent,
    NavMenuComponent,
    HomeComponent,
    ClientsSerivcesDataComponent,
    ClientsSerivcesSumComponent,
    FooterComponent
    //DetailsComponent,
    //MatPaginatorModule
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    HttpClientModule,
    FormsModule,
    NgbModule,
    //MatFormFieldModule,
    //MatInputModule,
    FontAwesomeModule,
    RouterModule.forRoot([
      { path: '', component: HomeComponent, pathMatch: 'full' },
      { path: 'clientsservice', component: ClientsSerivcesDataComponent },
      { path: 'clientsservicesum', component: ClientsSerivcesSumComponent, }
    ])
  ],
  providers: [ServicesComponent],
  bootstrap: [AppComponent]
})
export class AppModule { }
