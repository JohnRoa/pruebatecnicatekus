﻿using webappclientstekus.Classs;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webapitekus.models;
using Microsoft.Extensions.Caching.Memory;
using webapitekus.classs;

namespace webappclientstekus.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class ClientServicesController : ControllerBase
    {
        /// <summary>Get the list clients services.</summary>
        /// <returns>
        ///   <br /> A object from  GetMemoryandDataBaseCliSe whit model all clients and servivce by countries, and get elements neccesaty to load page
        /// </returns>
        [HttpGet("getlistclientsservices")]
        public object GetListClientsServices()
        {
            return this.GetMemoryandDataBaseCliSe();
        }

        /// <summary>Gets the servicesbycountry.</summary>
        /// <returns>
        ///   <br />A obect with list service by country summary
        /// </returns>
        [HttpGet("getservicesbycountry")]
        public object GetServicesbycountry()
        {
            return requestapi.invokeapi("getservicesbycountry", requestapi.Methods.GET);
        }

        /// <summary>Creates the service by client in specifi country</summary>
        /// <param name="model">The model to save</param>
        /// <returns>
        ///   <br /> A object whit model all clients and servivce by countries, and get elements neccesaty to load page update
        /// </returns>
        [HttpPost("createservicebyclient")]
        public object CreateServicebyClient([FromBody] List<modelreqclientservice> model)
        {
            return requestapi.invokeapi("createservicebyclient", requestapi.Methods.POST, model);
        }

        /// <summary>Creates the service by client in specifi country</summary>
        /// <param name="model">The model to save</param>
        /// <returns>
        ///   <br /> A object whit model all clients and servivce by countries, and get elements neccesaty to load page update
        /// </returns>
        [HttpDelete("deleteallrecordsserbycli")]
        public object DeleteAllRecordsServicebyClient()
        {
            return requestapi.invokeapi("deletealldataservice", requestapi.Methods.DELETE);
        }

        /// <summary>Gets the memoryand data base cli se.</summary>
        /// <returns>
        ///   <br />A object whit model all clients and servivce by countries, and get elements neccesaty to load page
        /// </returns>
        private object GetMemoryandDataBaseCliSe()
        {
            return requestapi.invokeapi("getclientesservices", requestapi.Methods.GET);
        }
    }
}
