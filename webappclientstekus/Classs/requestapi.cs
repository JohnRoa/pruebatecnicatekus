﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using webapitekus.classs;
//using System.Web.Script;

namespace webappclientstekus.Classs
{
    public static class requestapi
    {
        //Enum to parameter type of Method to request in API
        public enum Methods{ POST, GET, PUT, DELETE}


        /// <summary>Invoke to API the specified route o method</summary>
        /// <param name="route">The Method to make request</param>
        /// <param name="met">Type of method of request</param>
        /// <param name="model">The model parameter of method</param>
        /// <returns>
        ///   <br />Any object to need
        /// </returns>
        public static object invokeapi(string route, Methods met,  object model = null)
        {

            //Instance class of response to view
            object responsereq = null;
            //responseapi responsereq = new responseapi();
            //Concat URL especif
            string url = string.Concat(appsettings.urlapi, "/", route);
            //var url = $"http://localhost:8080/item/{id}";
            //Create http request with URL
            var request = (HttpWebRequest)WebRequest.Create(url);
            //Type of Method to send
            request.Method = met.Equals(Methods.GET) ? "GET" : met.Equals(Methods.POST) ? "POST" : met.Equals(Methods.PUT) ? "PUT" : "DELETE";
            //Format of content of request, in this case is a json
            request.ContentType = "application/json";
            //Method accept a json
            request.Accept = "application/json";
            try
            {
                //When method has params
                if (met.Equals(Methods.POST))
                {
                    //string model = ;
                    //string json = $"{{\"model\":\"{model}\"}}";
                    using (var streamWriter = new StreamWriter(request.GetRequestStream()))
                    {
                        streamWriter.Write(JsonConvert.SerializeObject(model));
                        streamWriter.Flush();
                        streamWriter.Close();
                    }
                }
                //Get response
                using (WebResponse response = request.GetResponse())
                {
                    //Get response as a stream
                    using (Stream strReader = response.GetResponseStream())
                    {
                        if (strReader == null) return null;
                        //Read response as a stream reader
                        using (StreamReader objReader = new StreamReader(strReader))
                        {
                            //responsereq = serializer.DeserializeObject(objReader.ReadToEnd());
                            //responsereq = serializer.Deserialize<responseapi>(objReader.ReadToEnd());
                            responsereq = objReader.ReadToEnd();
                            //responsereq = JsonSerializer.Deserialize<responseapi>(objReader.ReadToEnd());
                            //responsereq = JsonConvert.DeserializeObject<responseapi>(objReader.ReadToEnd());
                            //responseapi convertedObject = Activator.CreateInstance(res, responseapi);
                            //    Convert.ChangeType(res, typeof(responseapi));
                            ////string data = JsonConvert.SerializeObject(objReader.ReadToEnd());
                            //responsereq = JsonSerializer.Deserialize<responseapi>(JsonReader(res));
                            //string jsonString = File.ReadAllText(objReader.ReadToEnd());
                            //responsereq = JsonSerializer.se<responseapi>(jsonString);
                        }
                    }
                }
            }
            catch (WebException ex)
            {
                //(responsereq as responseapi). = new responseapi();
                //responsereq.mssg = ex.Message;
            }

            return responsereq;
        }
    }
}
