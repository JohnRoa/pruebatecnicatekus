USE [master]
GO
/****** Object:  Database [tekus]    Script Date: 31/10/2021 0:44:48 ******/
CREATE DATABASE [tekus] ON  PRIMARY 
( NAME = N'tekus', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER\MSSQL\DATA\tekus.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'tekus_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER\MSSQL\DATA\tekus_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [tekus].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [tekus] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [tekus] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [tekus] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [tekus] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [tekus] SET ARITHABORT OFF 
GO
ALTER DATABASE [tekus] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [tekus] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [tekus] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [tekus] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [tekus] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [tekus] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [tekus] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [tekus] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [tekus] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [tekus] SET  DISABLE_BROKER 
GO
ALTER DATABASE [tekus] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [tekus] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [tekus] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [tekus] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [tekus] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [tekus] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [tekus] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [tekus] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [tekus] SET  MULTI_USER 
GO
ALTER DATABASE [tekus] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [tekus] SET DB_CHAINING OFF 
GO
USE [tekus]
GO
/****** Object:  Table [dbo].[clients]    Script Date: 31/10/2021 0:44:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[clients](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[nit] [int] NOT NULL,
	[name] [varchar](250) NOT NULL,
	[email] [varchar](70) NOT NULL,
 CONSTRAINT [PK_clients] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[clients_ser_countries]    Script Date: 31/10/2021 0:44:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[clients_ser_countries](
	[idclient] [int] NOT NULL,
	[idservice] [int] NOT NULL,
	[idcountry] [smallint] NOT NULL,
 CONSTRAINT [PK_clients_ser_countries] PRIMARY KEY CLUSTERED 
(
	[idclient] ASC,
	[idservice] ASC,
	[idcountry] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[countries]    Script Date: 31/10/2021 0:44:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[countries](
	[id] [smallint] NOT NULL,
	[name] [varchar](100) NOT NULL,
 CONSTRAINT [PK_countries] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[services]    Script Date: 31/10/2021 0:44:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[services](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](300) NOT NULL,
	[priceperhour] [decimal](14, 2) NOT NULL,
 CONSTRAINT [PK_services] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[clients] ON 

INSERT [dbo].[clients] ([id], [nit], [name], [email]) VALUES (1, 900000000, N'HD SYSTEM SECURITY', N'hdsy@gmail.com')
INSERT [dbo].[clients] ([id], [nit], [name], [email]) VALUES (2, 91185550, N'SECURITY QA', N'jjroacas_05@hotmail.com')
INSERT [dbo].[clients] ([id], [nit], [name], [email]) VALUES (3, 900001001, N'ASSURANCE SEQA', N'asus@gmail.com')
INSERT [dbo].[clients] ([id], [nit], [name], [email]) VALUES (4, 900111111, N'KASR LABORATORY', N'ks@laboratory.com')
INSERT [dbo].[clients] ([id], [nit], [name], [email]) VALUES (5, 918888888, N'STAYSECURE S.A.S', N'ssecu@gmail.com')
INSERT [dbo].[clients] ([id], [nit], [name], [email]) VALUES (6, 912222222, N'SPAY SECURITY', N'sps@sps.com')
INSERT [dbo].[clients] ([id], [nit], [name], [email]) VALUES (7, 91185444, N'ASUS COLOMBIA', N'contac@asusco.com')
INSERT [dbo].[clients] ([id], [nit], [name], [email]) VALUES (8, 988888444, N'ASUS BELGICA', N'asusbel@gmail.com')
INSERT [dbo].[clients] ([id], [nit], [name], [email]) VALUES (9, 922222222, N'HUAWEI CATAR', N'huaweicatar@catar.com')
INSERT [dbo].[clients] ([id], [nit], [name], [email]) VALUES (10, 777777777, N'CATAR SERVICE ANALITYCS', N'catarser@anali.com')
INSERT [dbo].[clients] ([id], [nit], [name], [email]) VALUES (11, 333333333, N'ASSURANCE SEQA', N'asus@gmail.com')
INSERT [dbo].[clients] ([id], [nit], [name], [email]) VALUES (12, 966666666, N'TEKUS SERVICES', N'tekus@gmail.com')
INSERT [dbo].[clients] ([id], [nit], [name], [email]) VALUES (13, 800800800, N'CANADA SOFT', N'casof@hotmail.com')
INSERT [dbo].[clients] ([id], [nit], [name], [email]) VALUES (14, 91176590, N'VINISOFT', N'vinisoft@gmail.com')
INSERT [dbo].[clients] ([id], [nit], [name], [email]) VALUES (15, 998998998, N'SEC & ADV S.A.S', N'secadv@gmail.com')
INSERT [dbo].[clients] ([id], [nit], [name], [email]) VALUES (16, 566666666, N'LEON & CIA QA', N'leon@gmail.com')
INSERT [dbo].[clients] ([id], [nit], [name], [email]) VALUES (17, 999888777, N'PEGASUS ANALITYCS', N'pega@gmail.com')
INSERT [dbo].[clients] ([id], [nit], [name], [email]) VALUES (18, 444555777, N'POWER COMPUTER', N'powerc@gmail.com')
INSERT [dbo].[clients] ([id], [nit], [name], [email]) VALUES (19, 333444555, N'SOFTKEY SA', N'softkeysa@hotmail.com')
INSERT [dbo].[clients] ([id], [nit], [name], [email]) VALUES (20, 989989988, N'SPAIN VALIDATE SOFT', N'spainv@gmail.com')
INSERT [dbo].[clients] ([id], [nit], [name], [email]) VALUES (21, 566566566, N'ANDROID SOFT', N'androidcon@gmail.com')
INSERT [dbo].[clients] ([id], [nit], [name], [email]) VALUES (22, 545545545, N'ITALY QA', N'italyqa@gmail.com')
INSERT [dbo].[clients] ([id], [nit], [name], [email]) VALUES (23, 766766766, N'OPTIMUS SOFT', N'ops@gmail.com')
INSERT [dbo].[clients] ([id], [nit], [name], [email]) VALUES (24, 677677677, N'ULTIMATE SOFT', N'ultimatesoft@utlimate.com')
INSERT [dbo].[clients] ([id], [nit], [name], [email]) VALUES (25, 344344344, N'ANALITYC QA LION', N'aqlion@lionfrance.com')
INSERT [dbo].[clients] ([id], [nit], [name], [email]) VALUES (26, 323323323, N'MILENIUN QA', N'mileniunqa@gmail.com')
INSERT [dbo].[clients] ([id], [nit], [name], [email]) VALUES (27, 434434434, N'INTEGRATION SOFT', N'intesof@gmail.com')
SET IDENTITY_INSERT [dbo].[clients] OFF
GO
INSERT [dbo].[clients_ser_countries] ([idclient], [idservice], [idcountry]) VALUES (1, 3, 49)
INSERT [dbo].[clients_ser_countries] ([idclient], [idservice], [idcountry]) VALUES (2, 1, 1)
INSERT [dbo].[clients_ser_countries] ([idclient], [idservice], [idcountry]) VALUES (3, 2, 54)
INSERT [dbo].[clients_ser_countries] ([idclient], [idservice], [idcountry]) VALUES (4, 4, 1)
INSERT [dbo].[clients_ser_countries] ([idclient], [idservice], [idcountry]) VALUES (5, 1, 56)
INSERT [dbo].[clients_ser_countries] ([idclient], [idservice], [idcountry]) VALUES (6, 2, 54)
INSERT [dbo].[clients_ser_countries] ([idclient], [idservice], [idcountry]) VALUES (7, 4, 56)
INSERT [dbo].[clients_ser_countries] ([idclient], [idservice], [idcountry]) VALUES (8, 4, 32)
INSERT [dbo].[clients_ser_countries] ([idclient], [idservice], [idcountry]) VALUES (9, 4, 974)
INSERT [dbo].[clients_ser_countries] ([idclient], [idservice], [idcountry]) VALUES (10, 4, 56)
INSERT [dbo].[clients_ser_countries] ([idclient], [idservice], [idcountry]) VALUES (11, 2, 56)
INSERT [dbo].[clients_ser_countries] ([idclient], [idservice], [idcountry]) VALUES (12, 4, 57)
INSERT [dbo].[clients_ser_countries] ([idclient], [idservice], [idcountry]) VALUES (13, 1, 61)
INSERT [dbo].[clients_ser_countries] ([idclient], [idservice], [idcountry]) VALUES (14, 1, 1)
INSERT [dbo].[clients_ser_countries] ([idclient], [idservice], [idcountry]) VALUES (14, 1, 56)
INSERT [dbo].[clients_ser_countries] ([idclient], [idservice], [idcountry]) VALUES (15, 3, 43)
INSERT [dbo].[clients_ser_countries] ([idclient], [idservice], [idcountry]) VALUES (16, 3, 49)
INSERT [dbo].[clients_ser_countries] ([idclient], [idservice], [idcountry]) VALUES (17, 3, 49)
INSERT [dbo].[clients_ser_countries] ([idclient], [idservice], [idcountry]) VALUES (18, 1, 55)
INSERT [dbo].[clients_ser_countries] ([idclient], [idservice], [idcountry]) VALUES (19, 3, 43)
INSERT [dbo].[clients_ser_countries] ([idclient], [idservice], [idcountry]) VALUES (20, 1, 54)
INSERT [dbo].[clients_ser_countries] ([idclient], [idservice], [idcountry]) VALUES (21, 3, 32)
INSERT [dbo].[clients_ser_countries] ([idclient], [idservice], [idcountry]) VALUES (21, 5, 55)
INSERT [dbo].[clients_ser_countries] ([idclient], [idservice], [idcountry]) VALUES (22, 3, 974)
INSERT [dbo].[clients_ser_countries] ([idclient], [idservice], [idcountry]) VALUES (23, 1, 56)
INSERT [dbo].[clients_ser_countries] ([idclient], [idservice], [idcountry]) VALUES (23, 5, 54)
INSERT [dbo].[clients_ser_countries] ([idclient], [idservice], [idcountry]) VALUES (24, 2, 974)
INSERT [dbo].[clients_ser_countries] ([idclient], [idservice], [idcountry]) VALUES (24, 3, 86)
INSERT [dbo].[clients_ser_countries] ([idclient], [idservice], [idcountry]) VALUES (25, 3, 49)
INSERT [dbo].[clients_ser_countries] ([idclient], [idservice], [idcountry]) VALUES (26, 1, 55)
INSERT [dbo].[clients_ser_countries] ([idclient], [idservice], [idcountry]) VALUES (27, 5, 55)
GO
INSERT [dbo].[countries] ([id], [name]) VALUES (1, N'Antigua y Barbuda')
INSERT [dbo].[countries] ([id], [name]) VALUES (32, N'Bélgica')
INSERT [dbo].[countries] ([id], [name]) VALUES (43, N'Austria')
INSERT [dbo].[countries] ([id], [name]) VALUES (49, N'Alemania')
INSERT [dbo].[countries] ([id], [name]) VALUES (54, N'Argentina')
INSERT [dbo].[countries] ([id], [name]) VALUES (55, N'Brasil')
INSERT [dbo].[countries] ([id], [name]) VALUES (56, N'Chile')
INSERT [dbo].[countries] ([id], [name]) VALUES (57, N'Colombia')
INSERT [dbo].[countries] ([id], [name]) VALUES (61, N'Australia')
INSERT [dbo].[countries] ([id], [name]) VALUES (86, N'China')
INSERT [dbo].[countries] ([id], [name]) VALUES (93, N'Afganistán')
INSERT [dbo].[countries] ([id], [name]) VALUES (213, N'Argelia')
INSERT [dbo].[countries] ([id], [name]) VALUES (226, N'Burkina Faso')
INSERT [dbo].[countries] ([id], [name]) VALUES (229, N'Benin')
INSERT [dbo].[countries] ([id], [name]) VALUES (235, N'Chad')
INSERT [dbo].[countries] ([id], [name]) VALUES (237, N'Camerún')
INSERT [dbo].[countries] ([id], [name]) VALUES (238, N'Cabo Verde')
INSERT [dbo].[countries] ([id], [name]) VALUES (244, N'Angola')
INSERT [dbo].[countries] ([id], [name]) VALUES (257, N'Burundi')
INSERT [dbo].[countries] ([id], [name]) VALUES (267, N'Botsuana')
INSERT [dbo].[countries] ([id], [name]) VALUES (355, N'Albania')
INSERT [dbo].[countries] ([id], [name]) VALUES (357, N'Chipre')
INSERT [dbo].[countries] ([id], [name]) VALUES (359, N'Bulgaria')
INSERT [dbo].[countries] ([id], [name]) VALUES (374, N'Armenia')
INSERT [dbo].[countries] ([id], [name]) VALUES (375, N'Bielorrusia')
INSERT [dbo].[countries] ([id], [name]) VALUES (376, N'Andorra')
INSERT [dbo].[countries] ([id], [name]) VALUES (387, N'Bosnia y Herzegovina')
INSERT [dbo].[countries] ([id], [name]) VALUES (501, N'Belice')
INSERT [dbo].[countries] ([id], [name]) VALUES (591, N'Bolivia')
INSERT [dbo].[countries] ([id], [name]) VALUES (673, N'Brunéi')
INSERT [dbo].[countries] ([id], [name]) VALUES (855, N'Camboya')
INSERT [dbo].[countries] ([id], [name]) VALUES (880, N'Bangladés')
INSERT [dbo].[countries] ([id], [name]) VALUES (966, N'Arabia Saudita')
INSERT [dbo].[countries] ([id], [name]) VALUES (973, N'Baréin')
INSERT [dbo].[countries] ([id], [name]) VALUES (974, N'Catar')
INSERT [dbo].[countries] ([id], [name]) VALUES (975, N'Bután')
INSERT [dbo].[countries] ([id], [name]) VALUES (994, N'Azerbaiyán')
GO
SET IDENTITY_INSERT [dbo].[services] ON 

INSERT [dbo].[services] ([id], [name], [priceperhour]) VALUES (1, N'RECUPERACION DE DISCOS DUROS', CAST(5.00 AS Decimal(14, 2)))
INSERT [dbo].[services] ([id], [name], [priceperhour]) VALUES (2, N'ELABORACIÓN DE SOFTWARE', CAST(25.00 AS Decimal(14, 2)))
INSERT [dbo].[services] ([id], [name], [priceperhour]) VALUES (3, N'ANÁLISIS DE SOFTWARE', CAST(9.00 AS Decimal(14, 2)))
INSERT [dbo].[services] ([id], [name], [priceperhour]) VALUES (4, N'SOPORTE A USUARIO', CAST(3.00 AS Decimal(14, 2)))
INSERT [dbo].[services] ([id], [name], [priceperhour]) VALUES (5, N'DESAROLLO SOFTWARE', CAST(50.00 AS Decimal(14, 2)))
SET IDENTITY_INSERT [dbo].[services] OFF
GO
ALTER TABLE [dbo].[clients_ser_countries]  WITH CHECK ADD  CONSTRAINT [FK_clie_ser_country_clients] FOREIGN KEY([idclient])
REFERENCES [dbo].[clients] ([id])
GO
ALTER TABLE [dbo].[clients_ser_countries] CHECK CONSTRAINT [FK_clie_ser_country_clients]
GO
ALTER TABLE [dbo].[clients_ser_countries]  WITH CHECK ADD  CONSTRAINT [FK_clie_ser_country_countries] FOREIGN KEY([idcountry])
REFERENCES [dbo].[countries] ([id])
GO
ALTER TABLE [dbo].[clients_ser_countries] CHECK CONSTRAINT [FK_clie_ser_country_countries]
GO
ALTER TABLE [dbo].[clients_ser_countries]  WITH CHECK ADD  CONSTRAINT [FK_clie_ser_country_services] FOREIGN KEY([idservice])
REFERENCES [dbo].[services] ([id])
GO
ALTER TABLE [dbo].[clients_ser_countries] CHECK CONSTRAINT [FK_clie_ser_country_services]
GO
USE [master]
GO
ALTER DATABASE [tekus] SET  READ_WRITE 
GO
