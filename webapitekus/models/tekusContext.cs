﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using webapitekus.classs;
using webapitekus.models;

#nullable disable

namespace webapitekus.Models
{
    public partial class tekusContext : DbContext
    {
        public tekusContext()
        {
        }

        public tekusContext(DbContextOptions<tekusContext> options)
            : base(options)
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                //#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                //optionsBuilder.UseSqlServer("Data Source=(local);Initial Catalog=tekus; User Id=sa;Password=u.owner;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False");
                //Add conectionstring to dbcontext
                optionsBuilder.UseSqlServer(appsettings.conex);
            }
        }

        //Instance the models or clases for EntityFramework
        public virtual DbSet<clients_ser_countries> Clients_Ser_Countries { get; set; }
        public virtual DbSet<countries> Countries { get; set; }
        public virtual DbSet<services> Services { get; set; }

        public virtual DbSet<clients> Clients { get; set; }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "Modern_Spanish_CI_AS");
            //Composite when table has many keys
            modelBuilder.Entity<clients_ser_countries>().HasKey(k => new { k.idclient, k.idcountry, k.idservice });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
