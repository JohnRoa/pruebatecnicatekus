﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace webapitekus.models
{
    public class vievmodelclientservicecountry
    {
        public List<modelclientservice> clientssercount { get; set; }
        public List<countries> countries { get; set; }
        public List<services> services { get; set; }
    }
}
