﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webapitekus.Models;

namespace webapitekus.models
{
    public class viewmodelservicesbycountry
    {
        #region Attributes of class
        public int countofclien { get; set; }
        public int countofservice { get; set; }
        public List<countservicepercountry> countserpercountry { get; set; }
        #endregion

        //Instance Entity
        private tekusContext db = new tekusContext();

        /// <summary>Get Summary of services of countries</summary>
        /// <returns>
        ///   <br /> List of viewmodelservicesbycountry
        /// </returns>
        public viewmodelservicesbycountry getlistservicesbycountry()
        {
            //Instance all class
            viewmodelservicesbycountry response = new viewmodelservicesbycountry()
            {
                countofclien = db.Clients.Count(),
                countofservice = db.Services.Count()
            };
            if (db.Clients_Ser_Countries.Count() > 0)
            {
                countries coun = new countries();
                services ser = new services();
                clients cli = new clients();
                response.countserpercountry = new List<countservicepercountry>();

                db.Clients_Ser_Countries.AsEnumerable().GroupBy(x => x.idcountry).ToList().ForEach(country => response.countserpercountry.Add(new countservicepercountry
                {
                    namecountry = db.Countries.FirstOrDefault(co => co.id.Equals(country.Key)).name,
                    countofservice = db.Clients_Ser_Countries.Any(csc => csc.idcountry.Equals(country.Key)) ? db.Clients_Ser_Countries.Count(csc => csc.idcountry.Equals(country.Key)) : 0
                }));
            }
            return response;
        }
    }
}
