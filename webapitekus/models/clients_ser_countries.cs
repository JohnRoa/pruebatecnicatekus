﻿using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using webapitekus.classs;
using webapitekus.Models;

namespace webapitekus.models
{
    public class clients_ser_countries
    {
        #region Attributes of class
        [Required]
        [Key]
        [Range(1, 999999999)]
        public int idclient { get; set; }

        [Required]
        [Key]
        [Range(1, 999999999)]
        public int idservice { get; set; }

        [Required]
        [Key]
        [Range(1, 32767)]
        public short idcountry { get; set; }
        #endregion

        //Instance Entity
        private tekusContext db = new tekusContext();

        /// <summary>Get list of clients_ser_countries</summary>
        /// <returns>
        ///   <br /> List of clients_ser_countries
        /// </returns>
        public List<clients_ser_countries> getclientsservices()
        {

            return db.Clients_Ser_Countries.ToList();
        }

        /// <summary>Deleteallservices this instance.</summary>
        /// <returns>
        ///   <br />bool of confirm delete
        /// </returns>
        public bool deleteallservice()
        {
            try
            {
                //Begin Transaction
                db.Database.BeginTransaction();
                List<clients_ser_countries> csc = db.Clients_Ser_Countries.ToList();
                db.Clients_Ser_Countries.RemoveRange(db.Clients_Ser_Countries.ToList());
                db.SaveChanges();
                db.Clients.RemoveRange(db.Clients.ToList());
                db.SaveChanges();
                //Reset autoincrement
                //db.Database.CreateExecutionStrategy<clients>().Property(x => x.id).UseIdentityColumn(seed: 0, increment: 1);
                //Confirm transaction
                db.Database.CommitTransaction();
                this.CreateCommand("DBCC CHECKIDENT ('tekus.dbo.clients', RESEED, 0)");
                return true;
            }
            catch (Exception ex)
            {
                //Rollback of transaction when there is a error
                db.Database.RollbackTransaction();
            }
            return false;
        }

        private void CreateCommand(string queryString)
        {
            using (SqlConnection connection = new SqlConnection(appsettings.conex))
            {
                SqlCommand command = new SqlCommand(queryString, connection);
                command.Connection.Open();
                command.ExecuteNonQuery();
            }
        }
    }
}
