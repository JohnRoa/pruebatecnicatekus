﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using webapitekus.Models;

namespace webapitekus.models
{
    public class services
    {
        #region Attributes of class
        [Key]
        public int id { get; set; }

        [MaxLength(300)]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Field 'name' is required")]
        public string name { get; set; }

        public decimal priceperhour { get; set; }
        #endregion
        //Instance Entity
        private tekusContext db = new tekusContext();

        /// <summary>Get a service.</summary>
        /// <param name="_id">Parameter for searching one service.</param>
        /// <returns>
        ///   <br />
        /// </returns>
        public bool getService(int _id)
        {
            //Return true if service exists
            return db.Services.Any(c => c.id.Equals(_id)) ? true : false;
        }
    }
}
