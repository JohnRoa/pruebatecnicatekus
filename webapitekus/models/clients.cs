﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using webapitekus.Models;

namespace webapitekus.models
{
    public class clients
    {
        #region Attributes of class
        [Key]
        public int id { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Field 'nit' is required")]
        public int nit { get; set; }

        [MaxLength(250)]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Field 'name' is required")]
        public string name { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Field 'email' is required")]

        [MaxLength(50)]
        public string email { get; set; }
        #endregion
        //Instance Entity
        private tekusContext db = new tekusContext();

        /// <summary>Get a client.</summary>
        /// <param name="nit">Parameter for searching one client.</param>
        /// <returns>
        ///   <br />
        /// </returns>
        public bool getClient(int nit)
        {
            //Return true if client exists
            return db.Clients.Any(c => c.nit.Equals(nit)) ? true : false;
        }
    }
}
