﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webapitekus.Models;

namespace webapitekus.models
{
    public class modelclientservice
    {
        #region Attributes of class
        //public services _services { get; set; }
        //public clients _clients { get; set; }
        // public countries _countries { get; set; }
        //public clients _clients { get; set; }
        //public services _services { get; set; }
        public int idclient { get; set; }
        public string name { get; set; }
        public string email { get; set; }
        public int idservice { get; set; }
        public string service { get; set; }
        public short idcountry { get; set; }
        public string country { get; set; }
        public decimal priceperhour { get; set; }
        #endregion

        //Instance Entity
        private tekusContext db = new tekusContext();

        /// <summary>Get list of clients_ser_countries</summary>
        /// <returns>
        ///   <br /> List of viewmodelclientservice
        /// </returns>
        public vievmodelclientservicecountry getlistclientsservices()
        {
            //Instance all class
            vievmodelclientservicecountry response = new vievmodelclientservicecountry();
            response.countries = db.Countries.ToList();
            response.services = db.Services.ToList();

            if (db.Clients_Ser_Countries.Count() > 0)
                response.clientssercount = this.getlismodelclientservice();
            return response;
        }

        public List<modelclientservice> getlismodelclientservice()
        {
            //Fill Class
            List<modelclientservice> clientssercount = new List<modelclientservice>();
            List<countries> lcon = db.Countries.ToList();
            List<services> lser = db.Services.ToList();
            List<clients> lcli = db.Clients.ToList();

            db.Clients_Ser_Countries.ToList().ForEach(l =>
            {
                clientssercount.Add(new modelclientservice
                {
                    idclient = l.idclient,
                    name = lcli.FirstOrDefault(c => c.id.Equals(l.idclient)).name,
                    email = lcli.FirstOrDefault(c => c.id.Equals(l.idclient)).email,
                    idservice = l.idservice,
                    service = lser.FirstOrDefault(c => c.id.Equals(l.idservice)).name,
                    idcountry = l.idcountry,
                    country = lcon.FirstOrDefault(c => c.id.Equals(l.idcountry)).name,
                    priceperhour = lser.FirstOrDefault(c => c.id.Equals(l.idservice)).priceperhour
                });
            });

            return clientssercount;
        }
    }
}
