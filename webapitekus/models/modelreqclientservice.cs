﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using webapitekus.Models;

namespace webapitekus.models
{
    public class modelreqclientservice
    {
        #region Attributes of class
        [Required]
        [Range(1, 999999999)]
        public int nit { get; set; }

        [MaxLength(250)]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Field 'Name' is required")]
        public string name { get; set; }

        [MaxLength(70)]
        [RegularExpression(@"^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$", ErrorMessage = "Email field is not a valid e-mail address.")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Field 'Email' is required")]
        public string email { get; set; }

        [Required]
        [Range(1, 999999999)]
        public int idservice { get; set; }

        [Required]
        [Range(1, 32767)]
        public short idcountry { get; set; }
        public bool cache { get; set; }
        #endregion

        //Instance Entity
        private tekusContext db = new tekusContext();

        /// <summary>Save service to client in country.</summary>
        /// <returns>
        ///   <br /> return modelclientservice to load page again
        /// </returns>
        public bool saveclientservice()
        {
            //vievmodelclientservicecountry modelview = new vievmodelclientservicecountry();
            //modelclientservice getpmodel= new modelclientservice();
            try
            {
                List<clients> cl = new List<clients>();
                cl = db.Clients.ToList();
                //Init transaction
                db.Database.BeginTransaction();
                //Add to table  Clients_Ser_Countries item

                if (!cl.Any(i => i.nit.Equals(this.nit)))
                {
                    db.Clients.Add(new clients
                    {
                        nit = this.nit,
                        name = this.name.ToUpper(),
                        email = this.email.ToLower(),
                    });
                    db.SaveChanges();
                }//Update client
                else
                {
                    clients cli = cl.FirstOrDefault(i => i.nit.Equals(this.nit));
                    cli.name = this.name.ToUpper();
                    cli.email = this.email.ToLower();
                    db.Update(cli);
                    db.SaveChanges();
                }
                db.Clients_Ser_Countries.Add(new clients_ser_countries
                {
                    idclient = db.Clients.FirstOrDefault(i => i.nit.Equals(this.nit)).id,
                    idcountry = this.idcountry,
                    idservice = this.idservice
                });
                //Savw in the table  Clients_Ser_Countries item
                db.SaveChanges();
                //Commit transaction in table
                db.Database.CommitTransaction();
                //Return model
                //modelview = getpmodel.getlistclientsservices();
                return true;

            }
            catch (Exception ex)
            {
                db.Database.RollbackTransaction();
            }

            return false;
        }

        /// <summary>Validate if service exists to client</summary>
        /// <returns>
        ///   <br />firts clients_ser_countries
        /// </returns>
        public clients_ser_countries validateclientsservice()
        {
            clients cl = db.Clients.Any(c => c.nit.Equals(this.nit)) ? db.Clients.FirstOrDefault(c => c.nit.Equals(this.nit)) : null;
            if(cl != null)
                return db.Clients_Ser_Countries.Any(p => p.idclient.Equals(cl.id) && p.idcountry.Equals(this.idcountry) && p.idservice.Equals(this.idservice)) ? db.Clients_Ser_Countries.FirstOrDefault(p => p.idclient.Equals(cl.id) && p.idcountry.Equals(this.idcountry) && p.idservice.Equals(this.idservice)) : null;
            
            return null;
        }
    }
}
