﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using webapitekus.Models;

namespace webapitekus.models
{
    public class countries
    {
        #region Attributes of class
        [Key]
        public short id { get; set; }

        [MaxLength(100)]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Field 'name' is required")]
        public string name { get; set; }
        #endregion
        //Instance Entity
        private tekusContext db = new tekusContext();

        /// <summary>Get a country.</summary>
        /// <param name="_id">Parameter for searching one country.</param>
        /// <returns>
        ///   <br />
        /// </returns>
        public bool getCountry(short _id)
        {
            //Return true if country exists
            return db.Countries.Any(c => c.id.Equals(_id)) ? true : false;
        }
    }
}
