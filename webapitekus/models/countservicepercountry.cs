﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace webapitekus.models
{
    public class countservicepercountry
    {
        #region Attributes of class
        public string namecountry { get; set; }
        public int countofservice { get; set; }
        #endregion
    }
}
