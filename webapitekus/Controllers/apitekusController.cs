﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webapitekus.classs;
using webapitekus.models;

namespace webapitekus.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class apitekusController : ControllerBase
    {
        #region variables of controllers
        public responseapi resp = null;
        #endregion
        /// <summary>Get list of clients_ser_countries</summary>
        /// <returns>
        ///   <br /> List of clients_ser_countries
        /// </returns>
        [HttpGet("/getclientesservices")]
        public responseapi GetClientesServices()
        {
            resp = new responseapi();
            try
            {
                //Instance class
                //Get list of clients services of client
                resp.response = new modelclientservice().getlistclientsservices();
                resp.saved = true;
            }
            catch (Exception ex)
            {
                resp.mssg = ex.Message;
            }

            return resp;
        }

        /// <summary>Get list of clients_ser_countries</summary>
        /// <returns>
        ///   <br /> LIst of clients_ser_countries
        /// </returns>
        [HttpGet("/getservicesbycountry")]
        public responseapi GetServicesbyCountry()
        {
            resp = new responseapi();
            try
            {
                //Instance class
                //Get list of clients services countries
                resp.response = new viewmodelservicesbycountry().getlistservicesbycountry();
            }
            catch (Exception ex)
            {
                resp.mssg = ex.Message;
            }

            return resp;
        }

        /// <summary>Createservicebyclients is a method to create one service by client.</summary>
        /// <param name="model">Parameter to create service by client</param>
        /// <returns>
        ///   <br />List Object created
        /// </returns>
        [HttpPost("/createservicebyclient")]
        public List<responseapi> CreateServicebyClient([FromBody] List<modelreqclientservice> model)
        {
            //Instace class for response
            List<responseapi> resp= new List<responseapi>();
            try
            {
                //Instace clases for validations
                countries co = new countries();
                services ser = new services();
                clients cli = new clients();

                //Validate model is valid
                if (ModelState.IsValid)
                {
                    foreach (modelreqclientservice item in model)
                    {
                        //Validate model
                        if (!co.getCountry(item.idcountry))
                            resp.Add(new responseapi { mssg = string.Concat("The country ", item.idcountry.ToString(), ", No exists!"), saved = false });
                        else if (!ser.getService(item.idservice))
                            resp.Add(new responseapi { mssg = string.Concat("The service ", item.idservice.ToString(), ", No exists!"), saved = false });
                        //else if (!cli.getClient(model.nit))
                        //    res.mssg = string.Concat("The client ", model.nit.ToString(), " No exists!");
                        else if (item.validateclientsservice() != null)
                            resp.Add(new responseapi { mssg = string.Concat("The service ", item.idservice.ToString(), " to client ", item.nit.ToString(), ", already exists!"), saved = false });
                        else
                        {
                            //Create item of client, service, country
                            if(item.saveclientservice())
                            //if (resp.response != null)
                            {
                                resp.Add(new responseapi { mssg = string.Concat("Service ", item.idservice.ToString(), " to client ", item.nit.ToString(), " was success saved"), saved = true });
                            }
                        }
                    }

                    resp.Add(new responseapi { response = new modelclientservice().getlismodelclientservice(), mssg = "", saved = true }); 
                }
                else
                {
                    resp.Add(new responseapi { mssg = string.Join("\n", ModelState.Values) });
                }
            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                resp.Add(new responseapi { mssg = ex.Message });
            }

            return resp;
        }


        /// <summary>Deletes all table clientsservice</summary>
        /// <returns>
        ///   <br />
        /// </returns>
        [HttpDelete("/deletealldataservice")]
        public responseapi DeleteAll()
        {
            resp = new responseapi();
            clients_ser_countries clsc = new clients_ser_countries();
            if (clsc.deleteallservice())
            {
                resp.saved = true;
            }
            return resp;
        }
    }
}
