﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace webapitekus.classs
{
    public class responseapi
    {
        #region Attributes of class
        public string mssg { get; set; }
        public object response { get; set; }
        public bool saved { get; set; } = false;
        #endregion
    }
}
